# Digital Learning Content Repository v2.1
Developed by Shofiyyah Nadhiroh and Patricia Joanne.
Guided by Mrs. Mira Suryani.

## Issues
- Randomize selected courses.
- Beautify PPT slideshow interface.
- Find a way to get course name on course page.
- Fix static pages so they can be opened for dummy.
- Add pdf files. (files not yet given, manually add in admin panel)
- Admin: Beautify Admin Panel.
- Admin: Fix dropdown select to dynamic dropdown. *!important*
- Admin: Allowed file types to upload not yet setted. *!important*
- Admin: Deleted files still on the local folder.
- Admin: CRUD Admin (temporarily disabled)
- Add more courses in other departments! (for next version)

## History
- v1.0 Local launch
- v1.1 Mobile launch with XAMPP
- v2.0 Mobile launch with USBWebserver
- v2.1 Mobile launch with USBWebserver, change some UI